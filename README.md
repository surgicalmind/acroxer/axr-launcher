## AcroXeR Launcher for AcroXeR Anatomy Aesthetics
![](https://gitlab.com/surgicalmind/acroxer/axr-launcher/-/raw/main/AXRA_Launcher.png)

### Anouncements 
   - The new update does not support backward compatibility with previous versions. (~3.5.xx)
   - Please uninstall the previous version and reinstall the new version of the launcher.


## Lastest Version
### v3.7.0.0 (24.0129)
 - Fixed networking issues in AcroxeR server.
 - Fixed an error where the launch icon did not work properly.
  
  

## History

### v3.3.5 (23.0619)
 - Fix the error that update AcroXeR Launcher.

### v3.3.3 (23.0602)
 - Fix the error that update AcroXeR Launcher and AAA application.

### v3.3.2 (23.0602)
 - Server Stabilization
 - Fix the error that update AcroXeR Launcher.

### v3.3 (23.0510)
 - Seperate release version
 - Change banner image

### v3.2.1 (23.0510)
 - Add update log menu

### v3.2 (23.0509)
 - Add update log window

### v3.1 (23.0503)
 - Solve CPU overoccupation
 - UI improvement, Add animation




-- The end of file --
